library(DBI)
library(rstudioapi)
library(dplyr)

con <- dbConnect(odbc::odbc(),
                 driver = "Teradata",
                 dsn = "EDWD",
                 database = "ETL_LOAD_OWN",
                 uid = "madube",
                 pwd = rstudioapi::askForPassword("Database password"),
                 host = "EDWD")

# example query
dim_plan = dbGetQuery(con, "SELECT * FROM ETL_LOAD_OWN.DIM_PLAN;")

dim_plan %>% View()
